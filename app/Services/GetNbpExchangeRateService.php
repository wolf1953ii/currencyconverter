<?php

namespace App\Services;

use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class GetNbpExchangeRateService
{
    /**
     * @param string $currency
     * @return false|void
     */
    public function getExchangeRate(string $currency)
    {
        $response = $this->makeRequest(config('nbp-urls.tableA') . $currency . '/');

        if ($response->successful()) {
            $rates = $response->json('rates');

            return $rates[0]['mid'];
        } elseif ($response->status() == 404) {
            $response = $this->makeRequest(config('nbp-urls.tableB') . $currency . '/');
            if ($response->successful()) {
                $rates = $response->json('rates');

                return $rates[0]['mid'];
            }
        } else {
            return false;
        }
    }

    /**
     * @param string $url
     * @return PromiseInterface|Response|void
     */
    private function makeRequest(string $url)
    {
        try {
            return Http::acceptJson()->get($url);
        } catch (Throwable $exception) {
            Log::error($exception->getMessage());
        }
    }
}
