<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConverFromPlnRequest;
use App\Http\Requests\ConvertToPlnRequest;
use App\Services\GetNbpExchangeRateService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Throwable;

class CurrencyConverterController extends Controller
{
    /**
     * @var GetNbpExchangeRateService $nbpExchangeRateService
     */
    private $nbpExchangeRateService;

    public function __construct(GetNbpExchangeRateService $getNbpExchangeRateService)
    {
        $this->nbpExchangeRateService = $getNbpExchangeRateService;
    }

    /**
     * @param ConverFromPlnRequest $request
     * @return JsonResponse
     */
    public function convertFromPln(ConverFromPlnRequest $request): JsonResponse
    {
        $foreignCurrencyCode = $request->get('foreign_currency_code');
        $cashInPln = $request->get('cash_in_pln');

        try {
            $exchangeRate = $this->nbpExchangeRateService->getExchangeRate($foreignCurrencyCode);

            if ($exchangeRate) {
                $result = round(($cashInPln / $exchangeRate), 5);
            } else {
                $this->handleFalseExchangeRate();
            }
        } catch (Throwable $exception) {
            $this->handleException($exception);
        }

        return new JsonResponse([
            'message' => $cashInPln . ' PLN equals ' . $result . ' in ' . $foreignCurrencyCode,
            'base_currency' => 'PLN',
            'exchange_currency' => $foreignCurrencyCode,
            'exchange_rate' => $exchangeRate,
            'result' => $result,
        ]);
    }

    /**
     * @param ConvertToPlnRequest $request
     * @return JsonResponse
     */
    public function convertToPln(ConvertToPlnRequest $request): JsonResponse
    {
        $foreignCurrencyCode = $request->get('foreign_currency_code');
        $cashInForeignCurrency = $request->get('cash_in_foreign_currency');

        try {
            $exchangeRate = $this->nbpExchangeRateService->getExchangeRate($foreignCurrencyCode);

            if ($exchangeRate) {
                $result = round(($cashInForeignCurrency * $exchangeRate), 5);
            } else {
                $this->handleFalseExchangeRate();
            }
        } catch (Throwable $exception) {
            $this->handleException($exception);
        }

        return new JsonResponse([
            'message' => $cashInForeignCurrency . ' ' . $foreignCurrencyCode . ' equals ' . $result . ' in PLN',
            'base_currency' => $foreignCurrencyCode,
            'exchange_currency' => 'PLN',
            'exchange_rate' => $exchangeRate,
            'result' => $result,
        ]);
    }

    /**
     * @param Throwable $exception
     * @return JsonResponse
     */
    private function handleException(Throwable $exception): JsonResponse
    {
        Log::error($exception->getMessage());

        return new JsonResponse([
            'message' => 'Internal server error',
        ], 500);
    }

    /**
     * @return JsonResponse
     */
    private function handleFalseExchangeRate(): JsonResponse
    {
        return new JsonResponse([
            'message' => 'Conversion not possible',
        ]);
    }
}
