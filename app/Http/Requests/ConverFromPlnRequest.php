<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;
use PrinsFrank\Standards\Currency\ISO4217_Alpha_3;

class ConverFromPlnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'cash_in_pln' => 'required|numeric',
            'foreign_currency_code' => ['required', new Enum(ISO4217_Alpha_3::class)],
        ];
    }
}
