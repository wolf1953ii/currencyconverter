## Installation
1. Copy .env.example to .env
2. Run composer install or php composer.phar install
3. Run php artisan key:generate
4. Run php artisan serve

## API endpoints
This api serves two different endpoints first one for converting PLN to foreign currency and the second one is used to convert foreign currency to PLN

## Example calls
/api/convert-from-pln?foreign_currency_code=GBP&cash_in_pln=100<br>
/api/convert-to-pln?foreign_currency_code=GBP&cash_in_foreign_currency=1546

## API docs
1. Run php artisan serve
2. Visit http://localhost:8000/docs

## Tests
To run test type: php artisan test

## License
This project is licensed under the [MIT license](https://opensource.org/licenses/MIT).


