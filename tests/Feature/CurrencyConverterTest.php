<?php

namespace Tests\Feature;

use App\Services\GetNbpExchangeRateService;
use Mockery;
use Tests\TestCase;

class CurrencyConverterTest extends TestCase
{
    /**
     * Test conversion to PLN
     *
     * @return void
     */
    public function testConvertToPln(): void
    {
        //given
        $successControllerResponse = [
            "message" => "10 EUR equals 40 in PLN",
            "base_currency" => "EUR",
            "exchange_currency" => "PLN",
            "exchange_rate" => 4,
            "result" => 40,
        ];

        $mock = Mockery::mock(GetNbpExchangeRateService::class)->makePartial();
        $mock->shouldReceive('getExchangeRate')->andReturn(4);

        $this->app->bind(GetNbpExchangeRateService::class, function () use ($mock) {
            return $mock;
        });

        //when
        $response = $this->get('/api/convert-to-pln?foreign_currency_code=EUR&cash_in_foreign_currency=10');

        //then
        $response->assertStatus(200);
        $response->assertJson($successControllerResponse);
    }

    /**
     * Test conversion from PLN
     *
     * @return void
     */
    public function testConvertFromPln(): void
    {
        //given
        $successControllerResponse = [
            "message" => "40 PLN equals 10 in EUR",
            "base_currency" => "PLN",
            "exchange_currency" => "EUR",
            "exchange_rate" => 4,
            "result" => 10,
        ];

        $mock = Mockery::mock(GetNbpExchangeRateService::class)->makePartial();
        $mock->shouldReceive('getExchangeRate')->andReturn(4);

        $this->app->bind(GetNbpExchangeRateService::class, function () use ($mock) {
            return $mock;
        });

        //when
        $response = $this->get('/api/convert-from-pln?foreign_currency_code=EUR&cash_in_pln=40');

        //then
        $response->assertStatus(200);
        $response->assertJson($successControllerResponse);
    }
}
